package com.dicoding.submission.core.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    val movieId: String,
    val title: String,
    val overview: String,
    val vote_average: Double,
    val backdrop_path: String,
    val release_date: String,
    val isFavorite: Boolean

) : Parcelable