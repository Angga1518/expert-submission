package com.dicoding.submission.core.data.source.remote.network

import com.dicoding.submission.core.data.source.remote.response.ListMovieResponse
import retrofit2.http.GET

interface ApiService {
    @GET("movie?api_key=f26b2f4ec815589106fb203b45359c17&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1")
    suspend fun getList(): ListMovieResponse
}
