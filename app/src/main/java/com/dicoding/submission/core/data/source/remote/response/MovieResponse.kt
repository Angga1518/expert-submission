package com.dicoding.submission.core.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class MovieResponse(
    @field:SerializedName("id")
    val id: String,

    @field:SerializedName("title")
    val title: String,

    @field:SerializedName("overview")
    val overview: String,

    @field:SerializedName("release_date")
    val release_date: String,

    @field:SerializedName("vote_average")
    val vote_average: Double,

    @field:SerializedName("backdrop_path")
    val backdrop_path: String
)

