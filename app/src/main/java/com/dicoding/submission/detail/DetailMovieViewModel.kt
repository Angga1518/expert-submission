package com.dicoding.submission.detail

import androidx.lifecycle.ViewModel
import com.dicoding.submission.core.domain.model.Movie
import com.dicoding.submission.core.domain.usecase.MovieUseCase

class DetailMovieViewModel(private val movieUseCase: MovieUseCase) : ViewModel() {
    fun setFavoriteMovie(movie: Movie, newStatus:Boolean) =
        movieUseCase.setFavoriteMovie(movie, newStatus)
}

