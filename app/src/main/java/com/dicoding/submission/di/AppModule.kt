package com.dicoding.submission.di

import com.dicoding.submission.core.domain.usecase.MovieInteractor
import com.dicoding.submission.core.domain.usecase.MovieUseCase
import com.dicoding.submission.detail.DetailMovieViewModel
import com.dicoding.submission.favorite.FavoriteViewModel
import com.dicoding.submission.home.HomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val useCaseModule = module {
    factory<MovieUseCase> { MovieInteractor(get()) }
}

val viewModelModule = module {
    viewModel { HomeViewModel(get()) }
    viewModel { FavoriteViewModel(get()) }
    viewModel { DetailMovieViewModel(get()) }
}